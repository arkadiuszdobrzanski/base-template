<p align="center">
  <a href="https://weblider.eu">
    <img alt="Weblider" src="https://weblider.eu/wp-content/uploads/2019/08/logo-czarne@2.png" height="100">
  </a>
</p>
</p>

## Opis

Oparte na **Bedrock** , cóż więcej mówić. Sztosiwo.



## Ważne info

- Plik .env musi być w gitignore
- Używaj composera do instalowania wtyczek i wordpressa
- yarn build:production --watch <- używaj do mielenia scss na cssy. Pamietaj aby być w folderze swojego themesa


## Wymagane

- PHP >= 7.1
- Composer - [Install](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)

## .env



  - `DB_NAME` - Database name
  - `DB_USER` - Database user
  - `DB_PASSWORD` - Database password
  - `DB_HOST` - Database host
  - Optionally, you can define `DATABASE_URL` for using a DSN instead of using the variables above (e.g. `mysql://user:password@127.0.0.1:3306/db_name`)
- `WP_ENV` - Set to environment (`development`, `staging`, `production`)
- `WP_HOME` - Full URL to WordPress home (https://example.com)
- `WP_SITEURL` - Full URL to WordPress including subdirectory (https://example.com/wp)

## Dokumentacja

[https://roots.io/docs/bedrock/master/installation/](https://roots.io/docs/bedrock/master/installation/).

