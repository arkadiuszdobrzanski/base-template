//próba wdrożenia uniwersalnego axiosa do post i get
//eksperymentalne

import {getFormData} from './helpers';

const axios = require('axios');

function doGetSubmit(searchValue, url, fill)
{
    axios.get(url, {
        params: {
            data: searchValue
        }
    })
    .then(function (response) {
        console.log(response);
        $(fill).hide().fadeOut(250).html(response).fadeIn(350);
    })
    .catch(function (error) {
        console.log(error);
        $(fill).hide().fadeOut(250).html(error).fadeIn(350);
    })
    .then(function () {
        console.log('Data GET sended and received.');
    });
}

function doPostSubmit(url, fill, formId)
{
    var $form = $(formId);

  
    if (!$form.is('form')) {
        var serializedData = getFormData($form.closest('form'));
    } else {
        var serializedData = getFormData($form);
    }

    axios.post(url, {
        params: {
            data: serializedData
        }
    })
    .then(function (response) {
        console.log(response);
        $(fill).hide().fadeOut(250).html(response).fadeIn(350);
    })
      .catch(function (error) {
        console.log(error);
        $(fill).hide().fadeOut(250).html(error).fadeIn(350);
      })
      .then(function () {
        console.log('Data POST sended and received.');
      });
}


function submitData(method, searchValue, url, fill, formId)
{
    if (method === 'POST') {
        doPostSubmit(method, searchValue, url, fill, formId)
    } else if (method === 'GET') {
        doGetSubmit(method, searchValue, url, fill)
    } else {
        console.log('Niepoprawna metoda');
        return false;
    }

}
