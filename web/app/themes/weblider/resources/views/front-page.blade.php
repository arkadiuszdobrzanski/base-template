{{--
  Template Name: Strona główna
--}}
@extends('layouts.app')
@section('content')

    {{-- HERO   --}}
    @include('partials.page-header')

    {{--  Usługi  --}}
    @include('partials.services')

    {{--  PLan cenowy  --}}
    @include('partials.pricing')

    {{-- Kontaktowy formularz --}}
    @include('partials.contact-form')
@endsection
