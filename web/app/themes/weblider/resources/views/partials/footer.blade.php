<footer class="footer">
  <div class="container">
    <div class="widget-wrapper">
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <div class="footer-widget">
            <div class="logo">
              <a href="/"> <img height="66" width="180" class="img-fluid" src="{{ \App\asset_path('images/logo-weblider-czarne.png') }}" alt="Logo firmy weblider"> </a>
            </div>
            <p class="desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumrmod tempor invidunt.</p>
            <ul class="socials">

            </ul>
          </div>
        </div>
        <div class="col-lg-2 col-md-6">
          <div class="footer-widget">
            <h3>Menu</h3>
            <ul class="links">
              <li><a href="#home">Strona Główna</a></li>
              <li><a href="#about">O nas</a></li>
              <li><a href="#service">Usługi</a></li>
              <li><a href="#pricing">Cennik</a></li>
              <li><a href="#contact">Kontakt</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget">
            <h3>Informacje dodatkowe</h3>
            <ul class="links">
              <li><a href="#home">Ciasteczka</a></li>
              <li><a href="javascript:void(0)">Polityka bezpieczenśtwa</a></li>
              <li><a href="javascript:void(0)">RODO</a></li>
              <li><a href="javascript:void(0)">FAQ</a></li>
              <li><a href="javascript:void(0)">Deklaracja dostępności</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="footer-widget">
            <h3>Kontakt</h3>
            <ul class="info">
              <li>+48 123 123 123</li>
              <li>biuro@weblider.eu</li>
              <li>Weblider
                Labędzka 22>li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="copy-right">
      <p class="text-center">Designed and Developed by <a rel="noreferrer" href="https://weblider.eu" target="_blank" rel="nofollow">Weblider</a> </p>
    </div>
  </div>
</footer>
