<section id="pricing" class="pricing-section pt-50 container">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xxl-5 col-xl-6 col-lg-7">
        <div class="section-title text-center mb-60">
          <h1 class="mb-35 wow fadeInUp" data-wow-delay=".2s">Nasze plany cenowe</h1>
          <p class="wow fadeInUp" data-wow-delay=".4s">Wybierz plan dla siebie, a my spełnimy twoje oczekiwania!</p>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-lg-4 col-md-8 col-sm-10">
        <div class="single-pricing color-1">
          <div class="header">
            <h3 class="package-name">Standard</h3>
            <span class="price">3 000<small>zł</small> </span>
          </div>
          <ul>
            <li>AUDYT WCAG 2.1</li>
            <li>Projekt serwisu WCAG 2.1</li>
            <li>3 wersje kolorystyczne wysokiego kontrastu</li>
            <li>Moduł "CMS" do zarządzania treścią</li>
            <li>Moduł "Galeria zdjęć"</li>
            <li>Moduł "Banel Slider"</li>
            <li>Moduł "Formularz kontaktu</li>
            <li>Moduł "Mapa serwisu</li>
            <li>Moduł RODO</li>
            <li>Dostosowanie na poziomie AA</li>
            <li>Moduł "Zarządzanie plikami PDF</li>
            <li>Moduł "Wyszukiwarka"</li>
            <li>Podłączenie adresu www</li>
            <li>Pakiet bezpieczeństwa - Firewall</li>
            <li>Przeniesienie do 20 podstron</li>
          </ul>
          <a href="#contact" class="main-btn border-btn btn-hover">Kup teraz</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-8 col-sm-10">
        <div class="single-pricing color-2">
          <div class="header">
            <h3 class="package-name">Pakiet złoty</h3>
            <span class="price">5 000<small>zł</small> </span>
          </div>
          <ul>
            <li>AUDYT WCAG 2.1</li>
            <li>Deklaracja dostępności</li>
            <li>Projekt serwisu WCAG 2.1</li>
            <li>3 wersje kolorystyczne wysokiego kontrastu</li>
            <li>Moduł "CMS" do zarządzania treścią</li>
            <li>Moduł "Galeria zdjęć"</li>
            <li>Moduł "Baner Slider"</li>
            <li>Moduł "Formularz kontaktu"</li>
            <li>Moduł "Mapa serwisu"</li>
            <li>Moduł RODO</li>
            <li>Dostowanie na poziomie AA</li>
            <li>Moduł "Zarządzanie plikami PDF"</li>
            <li>Moduł "POP-UP" wyskakujące okienko informacyjne</li>
            <li>Moduł "Wyszukiwarka"</li>
            <li>Podłączenie adresu www</li>
            <li>Pakiet bezpieczeństwa - Firewall</li>
            <li>Przeniesienie do 50 podstron</li>
          </ul>
          <a href="#home" class="main-btn btn-hover">Kup teraz</a>
        </div>
      </div>
      <div class="col-lg-4 col-md-8 col-sm-10">
        <div class="single-pricing color-3">
          <div class="header">
            <h3 class="package-name">Pakiet Premium</h3>
            <span class="price">7 000<small>zł</small> </span>
          </div>
          <ul>
            <li>AUDYT WCAG 2.1</li>
            <li>DEKLARACJA DOSTĘPNOŚCI</li>
            <li>Projekt serwisu WCAG 2.1</li>
            <li>3 wersje kolorystyczne wysokiego kontrastu</li>
            <li>Moduł "CMS" do zarządzania treścią</li>
            <li>Moduł "Galeria zdjęć"</li>
            <li>Moduł "Baner Slider"</li>
            <li>Moduł "Formularz kontaktu"</li>
            <li>Moduł "Mapa serwisu"</li>
            <li>Moduł RODO</li>
            <li>Dostosowanie na poziomie AA</li>
            <li>Moduł "Zarządzanie plikami PDF"</li>
            <li>Moduł "POP-UP" wyskakujące okienko informacyjne</li>
            <li>Moduł "Wyszukiwarka"</li>
            <li>Podłączenie adresu wwww</li>
            <li>Pakiet bezpieczeństwa - Firewall</li>
            <li>Przeniesienie do 100 podstron</li>
          </ul>
          <a href="#service" class="main-btn border-btn btn-hover">Kup teraz</a>
        </div>
      </div>
    </div>
  </div>
    <div id="packages" class="packages container justify-content-around d-flex pt-150 row">
      <div class="first-package col-lg-12 col-md-12 col-sm-12 col-xxl-5">
        <h2 class="text-center mb-50">Podstawowy pakiet gwarancyjny</h2>
        <li>jednorazowe sprawdzenie, wprowadzonych treści na stronę internetową w zakresie 5 wytypowanych kluczowych podstron strony internetowej;</li>
        <li>wskazanie elementów niezgodnych ze standardem WCAG 2.1 (poziom AA);</li>
        <li>przygotowanie raportu z audytu do zamieszczenia w deklaracji dostępności</li>
        <p class="mt-50">Koszt: 750 zł/rocznie</p>
      </div>
      <div class="secondary-package col-lg-12 col-md-12 col-sm-12 col-xxl-5">
        <h2 class="text-center mb-50">Rozszerzony pakiet gwarancyjny</h2>
        <li>jednorazowe sprawdzenie wprowadzonych treści na stronę internetową (do 7 wytypowanych kluczowych podstron serwisu);</li>
        <li>wskazanie elementów niezgodnych z WCAG 2.1 (poziom AA);</li>
        <li>propozycje naprawy błędów;</li>
        <li>przygotowanie raportu z audytu do zamieszczenia w deklaracji dostępności;</li>
        <li>opinia o dostępności strony internetowej dla instytucji kontrolującej w przypadku kontroli i zastrzeżeń.</li>
        <p class="mt-50">Koszt: 1400 zł/rocznie</p>
      </div>
    </div>
    <div class="templates row">
      <div class="first-template"></div>
      <div class="secondary-template"></div>
      <div class="third-template"></div>
      <div class="fourth-template"></div>
    </div>
</section>
