<header class="header">
  <div class="navbar-area">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-12">
          <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="index.html">
              <img src="{{ \App\asset_path('images/logo-weblider-czarne.png') }}" height="30" width="180" alt="Logo naszej storny" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="toggler-icon"></span>
              <span class="toggler-icon"></span>
              <span class="toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
              <ul id="nav" class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="page-scroll" href="#home">Strona główna</a>
                </li>
                <li class="nav-item">
                  <a class="page-scroll" href="#service">Usługi</a>
                </li>
                <li class="nav-item">
                  <a class="page-scroll" href="#about">O nas</a>
                </li>

                <li class="nav-item">
                  <a class="page-scroll" href="#pricing">Cennik</a>
                </li>
                <li class="nav-item">
                  <a class="page-scroll" href="#contact">Kontakt</a>
                </li>
              </ul>
            </div>
            <!-- navbar collapse -->
          </nav>
          <!-- navbar -->
        </div>
      </div>
      <!-- row -->
    </div>
    <!-- container -->
  </div>
  <!-- navbar area -->
</header>
