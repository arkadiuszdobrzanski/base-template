<section id="home" class="hero-section img-bg container">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-xl-6">
        <div class="hero-content">
          <h1 class="wow fadeInUp" data-wow-delay=".2s">WCAG 2.1</h1>
          <p class="wow fadeInUp" data-wow-delay=".4s">
            Serwisy internetowe zgodne z wytycznymi dotyczących dostępności treści internetowych
          </p>
          <a href="#service" rel="nofollow" class="main-btn btn-hover wow fadeInUp" data-wow-delay=".6s">Sprawdź ofertę</a>
        </div>
      </div>
      <div class="col-xl-6 col-lg-5">
        <div class="hero-img wow fadeInUp" data-wow-delay=".5s">
        </div>
      </div>
    </div>
  </div>
</section>
