<?php


namespace App\Services;

use App\Services\OptionsAdmin\ACFHooks;
use App\Services\OptionsAdmin\FooterOptionsService;
use App\Services\OptionsAdmin\HeaderOptionsService;
use App\Services\DefaultService;

class OptionsAdminService extends DefaultService
{
    public function __construct()
    {
        acf_add_options_page(array(
            'page_title'    => 'Weblider - ustawienia templatki',
            'menu_title'    => 'Ustawienia templatki',
            'menu_slug'     => 'weblider-theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false,
            'icon_url' => 'https://weblider.eu/wp-content/themes/weblider/img/favicon/favicon.ico',
        ));
        $this->activateFooterOptions();
        $this->activateHeaderOptions();
        require 'OptionsAdmin/ACFHooks.php';
    }

    public function activateHeaderOptions(): FooterOptionsService
    {
        return new FooterOptionsService();
    }

    public function activateFooterOptions(): HeaderOptionsService
    {
        return new HeaderOptionsService();
    }
}
