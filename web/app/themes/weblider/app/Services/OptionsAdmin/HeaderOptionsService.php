<?php

namespace App\Services\OptionsAdmin;

class HeaderOptionsService
{
    public function __construct()
    {
        acf_add_options_sub_page(array(
            'page_title'    => 'Ustawienia nagłówka',
            'menu_title'    => 'Nagłówek',
            'parent_slug'   => 'weblider-theme-general-settings',
        ));
    }
}
