<?php

namespace App\Controllers;

use App\Services\WordpressPostsService;
use Sober\Controller\Controller;

class FrontPage extends Controller
{
    protected $acf = true;

    private $wpPost;

    public function getImage()
    {
        return get_field('image');
    }

    public function wpposts()
    {
        return $this->wpPost = new WordpressPostsService();
    }
}
